$(document).ready(function(){
    $("#sv-widget").load("https://glen-themes.gitlab.io/widgets/updt/01/sdv.html");
});

$(window).load(function(){
    var icon_url = getComputedStyle(document.documentElement)
                  .getPropertyValue("--Widget-Image-URL"),
                  
        the_icon = '<img class="icon-image" src='+icon_url+'>',
            
        the_text = getComputedStyle(document.documentElement)
                  .getPropertyValue("--Widget-Text");
            
    $(".sun-s").html(the_icon);
    $(".the-text").html(the_text);
	$("#sv-widget").css("visibility","visible");
});
